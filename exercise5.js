/**
 * input: số có 2 chữ số
 * 
 * to do: tìm số hàng đơn vị = input%10; số hàng chục = input/10 lấy phần nguyên
 * 
 * output: tổng số hàng đơn vị và hàng chục
 */

var input = 23;
var oneUnit = input%10;
var tenUnit = Math.floor(input/10);
var sum = oneUnit + tenUnit;

console.log('Tong cac ky so cua',input,'la:',sum);