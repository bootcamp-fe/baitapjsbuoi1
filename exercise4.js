/**
 * input: chiều dài, chiều rộng của hcn
 * 
 * todo: diện tích = dài*rộng; chu vi = (dài + rộng )*2
 *
 * output: diện tích
 */

var width = 20;
var length = 10;

var area = width*length;
console.log('Dien tich hinh chu nhat la: ',area);
var perimeter = (width + length)*2;
console.log('Chu vi hinh chu nhat la:',perimeter);